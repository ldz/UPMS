/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50624
Source Host           : localhost:3306
Source Database       : test

Target Server Type    : MYSQL
Target Server Version : 50624
File Encoding         : 65001

Date: 2015-10-13 16:20:08
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `LOGIN_NAME` varchar(100) DEFAULT NULL,
  `LOGIN_PWD` longtext,
  `COMPANY_ID` int(11) DEFAULT NULL,
  `SHOW_NAME` varchar(100) DEFAULT NULL,
  `ORG_ID` int(11) DEFAULT NULL,
  `ORG_NAME` varchar(100) DEFAULT NULL,
  `EMAIL` longtext COMMENT '邮箱，必须唯一，用作密码找回',
  `VER_EMAIL_FLAG` varchar(5) DEFAULT NULL COMMENT 'DICT_GLOBAL_YESNO 是Y，否N',
  `VER_EMAIL_DATE` datetime DEFAULT NULL,
  `FIRST_LANG` varchar(50) DEFAULT NULL,
  `SEL_THEME` varchar(50) DEFAULT NULL,
  `FIRST_VISIT_DT` datetime DEFAULT NULL,
  `PRE_VISIT_DT` datetime DEFAULT NULL,
  `LAST_VISIT_DT` datetime DEFAULT NULL,
  `LAST_ERR_DT` datetime DEFAULT NULL,
  `FIRST_VISIT_IP` varchar(100) DEFAULT NULL,
  `PRE_VISIT_IP` varchar(100) DEFAULT NULL,
  `LAST_VISIT_IP` varchar(100) DEFAULT NULL,
  `LAST_ERR_IP` varchar(100) DEFAULT NULL,
  `LOGIN_COUNT` int(11) DEFAULT NULL,
  `EMPLOYEE_FLAG` varchar(5) DEFAULT NULL COMMENT 'DICT_GLOBAL_YESNO 是Y，否N',
  `ONLINE_STATUS` varchar(5) DEFAULT NULL COMMENT 'DICT_SYS_ONLINE_STATUS：ONL在线、OFL离线、HID隐身',
  `TELEPHONE` varchar(50) DEFAULT NULL,
  `MP` varchar(50) DEFAULT NULL,
  `MP_VAL_FLAG` varchar(5) DEFAULT NULL COMMENT 'DICT_GLOBAL_YESNO 是Y，否N',
  `MP_VAL_TIME` datetime DEFAULT NULL,
  `USER_TYPE` varchar(5) DEFAULT NULL COMMENT 'DICT_SYS_USER_TYPE：COM企业客户、AGT代理商、PEF个人免费客户、PEM个人会员客户、PEV个人VIP客户',
  `QUESTION_FLAG` varchar(5) DEFAULT NULL COMMENT 'DICT_GLOBAL_YESNO 是Y，否N',
  `MISC_DESC` longtext,
  `STATUS` varchar(5) DEFAULT NULL COMMENT '状态：DICT_GLOBAL_STATUS 有效V、无效I、草稿D、待审核W',
  `CREATE_TIME` datetime DEFAULT NULL,
  `CREATE_OPER_ID` int(11) DEFAULT NULL,
  `CREATE_OPER_NAME` varchar(50) DEFAULT NULL,
  `LAST_MOD_TIME` datetime DEFAULT NULL,
  `LAST_MOD_OPER_ID` int(11) DEFAULT NULL,
  `LAST_MOD_OPER_NAME` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_Reference_3` (`COMPANY_ID`) USING BTREE,
  KEY `FK_Reference_4` (`ORG_ID`) USING BTREE,
  CONSTRAINT `sys_user_ibfk_1` FOREIGN KEY (`COMPANY_ID`) REFERENCES `sys_company` (`ID`),
  CONSTRAINT `sys_user_ibfk_2` FOREIGN KEY (`ORG_ID`) REFERENCES `sys_org_tree` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8 COMMENT='用户，与compnay用户共用，包括系统的用户，个人客户，企业客户';

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES ('1', 'lyg945', 'lyg945', '1', 'lyg945', null, '无线通讯', '544872316@qq.com', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '13564672700', '13564672700', null, null, '1', null, null, '1', null, null, null, null, null, null);
INSERT INTO `sys_user` VALUES ('2', 'zhangsan', 'E10ADC3949BA59ABBE56E057F20F883E', '1', '张三', '1', '无线通讯', '544872316@qq.com', '1', '2014-09-02 20:12:52', '英语', null, null, null, null, null, null, null, null, null, '0', null, null, '13564672700', '13564672700', null, null, null, null, null, 'V', null, null, null, null, null, null);
INSERT INTO `sys_user` VALUES ('3', 'lisi', 'E10ADC3949BA59ABBE56E057F20F883E', '1', '李四', '1', '无线通讯', '544872316@qq.com', '1', '2014-09-02 20:12:52', '韩语', null, null, null, null, null, null, null, null, null, '0', null, null, '13564672700', '13564672700', null, null, null, null, null, 'I', null, null, null, '2015-01-05 14:54:29', null, null);
INSERT INTO `sys_user` VALUES ('4', 'test', 'E10ADC3949BA59ABBE56E057F20F883E', '1', '游客', '1', '无线通讯', '544872316@qq.com', '1', '2014-09-02 20:12:52', '法语', null, null, null, null, null, null, null, null, null, '0', null, null, '13564672700', '13564672700', null, null, null, null, null, 'V', null, null, null, null, null, null);
INSERT INTO `sys_user` VALUES ('5', '123', 'E10ADC3949BA59ABBE56E057F20F883E', '1', 'guest', '1', '无线通讯', '544872316@qq.com', null, null, null, null, null, null, null, null, null, null, null, null, '0', 'N', null, '13564672700', '13564672700', null, null, 'PEF', null, '放大', 'V', null, null, null, '2014-09-14 16:32:43', null, null);
INSERT INTO `sys_user` VALUES ('12', 'fangke', 'E10ADC3949BA59ABBE56E057F20F883E', '1', '访客', '2', '无线通讯', '544872316@qq.com', null, null, null, null, null, null, null, null, null, null, null, null, '0', 'Y', null, '13564672700', '13564672700', null, null, 'COM', 'Y', 'dasdad', 'V', null, null, null, '2014-09-14 19:36:34', null, null);
INSERT INTO `sys_user` VALUES ('17', 'aaaaa', 'testtest', '1', 'aaaaa', null, null, '544872316@qq.com', null, null, null, null, null, null, null, null, null, null, null, null, null, '1', null, '13564672700', '13564672700', null, null, '1', null, null, '1', null, null, null, null, null, null);
INSERT INTO `sys_user` VALUES ('22', 'ChinaSailing', 'ChinaSailing', '1', 'ChinaSailing', null, '无线通讯', '544872316@qq.com', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '13564672700', '13564672700', null, null, '1', null, null, '1', null, null, null, null, null, null);
INSERT INTO `sys_user` VALUES ('23', 'Copyright', 'Copyright', '1', 'Copyright', null, '无线通讯', '544872316@qq.com', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '13564672700', '13564672700', null, null, '1', null, null, '1', null, null, null, null, null, null);
INSERT INTO `sys_user` VALUES ('25', '2222', '111111', '1', '2222', null, '无线通讯', '544872316@qq.com', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '13564672700', '13564672700', null, null, '1', null, null, '1', null, null, null, null, null, null);
INSERT INTO `sys_user` VALUES ('26', 'asd', '111111', null, 'asd', '2', null, '544872316@qq.com', null, null, null, null, null, null, null, null, null, null, null, null, null, '2', null, '1111', '18001670539', null, null, '1', null, null, '2', null, null, null, null, null, null);
